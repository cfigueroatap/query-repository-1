 SELECT o.updated_at,
    o.tx,
    o.service,
    o.external_ref,
    o.api_response ->> 'message'::text AS error_message,
    (o.api_response -> 'metadata'::text) ->> 'message'::text AS metadata_message,
    o.operation_status_id,
    0 as card_type
   FROM prepaid.operation o
  WHERE (o.operation_status_id = ANY (ARRAY[1::bigint, 3::bigint])) AND (o.service = ANY (ARRAY['create-account-card'::text, 'external-authorization-request'::text]));

  

  LG004: account insufficient balance