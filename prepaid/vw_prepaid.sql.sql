 SELECT c.id AS card_id,
    c.account_id,
    c.created_at AS card_created_at,
    c.card_type,
    c.card_status_id,
    t.transaction_id,
    t.transaction_type_id,
    m.id AS movement_id,
    m.origin_id,
    m.created_at,
    m.metadata,
    m.amount,
    m.metadata ->> 'isInternational'::text AS isinternational,
    pt.external_ref,
    pt.status,
    pt.type,
    pt.commerce_name,
    pt.reason_id AS reason_id_cancel
   FROM prepaid.card c
     LEFT JOIN prepaid.transaction pt ON c.id = pt.card_id
     LEFT JOIN ledger.movement m ON pt.external_request_id = (m.metadata ->> 'externalRequestId'::text)
     LEFT JOIN ledger.transaction t ON m.id = t.movement_id;