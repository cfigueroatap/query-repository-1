 SELECT to_char(t.created_at - '03:00:00'::time without time zone::interval, 'HH24:MI:SS'::text) AS transaction_hour,
    t.transaction_id,
    to_char(t.created_at - '03:00:00'::time without time zone::interval, 'YYYY-MM-DD'::text) AS transaction_day,
    t.created_at - '03:00:00'::time without time zone::interval AS transaction_date,
    m.amount,
    t.transaction_type_id,
    tt.description,
    p.payment_key
   FROM ledger.movement m
     LEFT JOIN ledger.transaction t ON m.id = t.movement_id
     LEFT JOIN ledger.transaction_type tt ON t.transaction_type_id = tt.transaction_type_id
     LEFT JOIN beyond.payments p ON (m.metadata ->> 'idTransaccion'::text) = p.transaction_id::text
  WHERE t.transaction_type_id IN (32, 33, 34);