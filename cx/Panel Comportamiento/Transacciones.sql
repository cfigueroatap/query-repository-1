SELECT
	bl.blacklist_active AS blacklist,
    bl2.blacklist_active as blacklist_target_t2t,
    o1.name,
    o1.external_ref,
    o2.name as t2t_target_name,
    t1.movement_id,
    t1.transaction_id,
    m1.created_at,
    to_char(m1.created_at - '00:00:00'::time without time zone::interval, 'YYYY-MM-DD'::text) as transaction_day,
    to_char(m1.created_at - '00:00:00'::time without time zone::interval, 'HH24:MI:SS'::text) as transaction_hour,
    m1.details,
    json_extract_path_text(m1.metadata,'code') as promocode,
    m1.amount,
    m1.target,
    json_extract_path_text(m1.metadata,'is_qr') as is_qr,
    SPLIT_PART(SPLIT_PART(json_extract_path_text(metadata, 'ticket' , 'Ticket'),'Medidor Nro. ',2),'"',1) AS medidor_mide,
    json_extract_path_text(m1.metadata ,'last_token') as comprobante_mide,
    json_extract_path_text(m1.metadata, 'operation', 'operation_response' , 'company_client_id')  as celular_recargado
FROM tap_l1_pg_ledger_public.movement m1
LEFT JOIN tap_l1_pg_ledger_public.transaction t1 ON m1.id = t1.movement_id
LEFT JOIN tap_l1_pg_ledger_public.origin o1 ON t1.origin_id = o1.origin_id
LEFT JOIN tap_l1_pg_ledger_public.origin o2 ON m1.target = o2.external_ref
LEFT JOIN tap_l1_pg_app_public.account ac1 ON o1.external_ref = ac1.account_hash
LEFT JOIN tap_l1_pg_app_public.person ap1 ON ac1.account_id = ap1.account_id
LEFT JOIN (
    with blacklist1 as
	(select external_ref, max(blacklist_id) AS blacklist_id
     FROM tap_l1_pg_auth_public.blacklist
    group by external_ref
    )
    SELECT *
    FROM tap_l1_pg_auth_public.blacklist
    WHERE blacklist_id IN (
    SELECT blacklist_id FROM blacklist1)
    )
    AS bl ON o1.external_ref = bl.external_ref
LEFT JOIN (
    with blacklist1 as
	(select external_ref, max(blacklist_id) AS blacklist_id
     FROM tap_l1_pg_auth_public.blacklist
    group by external_ref
    )
    SELECT *
    FROM tap_l1_pg_auth_public.blacklist
    WHERE blacklist_id IN (
    SELECT blacklist_id FROM blacklist1)
    )
    AS bl2 ON o2.external_ref = bl.external_ref
WHERE
        ap1.document_id = 'x' --DNI
        OR o1.external_ref = '1f971270-4d3b-40aa-806a-f24086e91968'
        OR ap1.name = 'x'
        OR ap1.phone = 'x'
        OR o1.document = 'x' --CUIT
        OR ap1.email = 'x'
        OR ac1.device_id = 'x'
ORDER BY external_ref, m1.created_at DESC