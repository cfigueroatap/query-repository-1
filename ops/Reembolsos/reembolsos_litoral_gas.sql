WITH first AS (
SELECT 	external_ref,
        min(m1.created_at) AS first_transaction
FROM ledger.movement m1
LEFT JOIN ledger.transaction t1 ON m1.id = t1.movement_id
LEFT JOIN ledger.origin o1 ON t1.origin_id = o1.origin_id
WHERE
   	 	(transaction_type_id IN (32,33) AND details ILIKE '%LITORAL%')
	OR (transaction_type_id = 7 AND o1.name = 'Litoral Gas')
	OR (transaction_type_id IN (1,22) AND target = 'LITORAL GAS')
GROUP BY external_ref
   )
   
   SELECT 
	m1.created_at AS transaction_date, 
	t1.transaction_id AS transaction, 
	a1.phone , 
	o1.external_ref AS extref_origin,
	CASE WHEN 0.8*abs(m1.amount) <= 1000 THEN 0.8*abs(m1.amount)
	ELSE 1000
	END AS amount,
	'Bonificacion Litoral Gas' AS message
FROM ledger.movement m1
LEFT JOIN ledger.transaction t1 
	ON m1.id = t1.movement_id
LEFT JOIN ledger.origin o1 
	ON t1.origin_id = o1.origin_id
LEFT JOIN app.account a1 
	ON o1.external_ref = a1.account_hash
LEFT JOIN first f 
	ON o1.external_ref = f.external_ref
WHERE 
	f.first_transaction = m1.created_at
	AND date(m1.created_at) = '2022-01-19'
	AND o1.external_ref <> 'Litoral Gas' 
	AND m1.metadata ->> 'code' IS NULL
	AND ((transaction_type_id IN (32,33) AND details ILIKE '%LITORAL%')
	OR (transaction_type_id = 7 AND o1.name = 'Litoral Gas')
	OR (transaction_type_id IN (1,22) AND target = 'LITORAL GAS'))
GROUP BY transaction_date, transaction, phone, extref_origin, m1.amount