WITH x1 AS (
SELECT
    m1.created_at AS transaction_date,
	t1.transaction_id AS transaction,
	a1.phone,
	o1.external_ref AS extref_origin,
	'Premio por tu compra' AS message,
    0.02*abs(m1.amount) as amount
FROM ledger.movement m1
LEFT JOIN ledger.transaction t1
	ON m1.id = t1.movement_id
LEFT JOIN ledger.origin o1
	ON t1.origin_id = o1.origin_id
LEFT JOIN app.account a1
	ON o1.external_ref = a1.account_hash
WHERE t1.transaction_type_id = 30
	AND date(m1.created_at) = '2022-01-25'
	AND o1.external_ref <> 'gp-wc'
)

SELECT
    date(transaction_date),
	phone,
	extref_origin,
	'Premio por tu compra' AS message,
    CASE WHEN sum(amount) <= 600 THEN sum(amount)
    ELSE 600
    END AS amount
FROM x1
GROUP BY date(transaction_date), phone, extref_origin, message
ORDER BY amount DESC